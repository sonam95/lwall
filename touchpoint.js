//alert("hi");
var datatable = (function() {//this line store the function to the seperate variable.
var toptouchpointTable = null;
var containerToAppendTo = null;
var boxName1 = "Top Five Branches";//creating a variable for storing the value
var topdata = [];//empty data

function tableElement(data5, tableContainer){//this is a inner function.
console.log(data5);//print the data
topdata = data5; 
containerToAppendTo = document.getElementById("tableContainer");//append the containerID 
console.log("containerToAppendTo", containerToAppendTo)
toptouchpointTable = document.createElement('table');//creating a table 
toptouchpointTable.id = "tableContainer1";
document.getElementById("tableContainer").innerHTML = boxName1;//display the variable value
    createHeader();//call the function
    createRow();//call the function
}
var thead = document.createElement('thead');//creating table header
var tbody = document.createElement('tbody');//creating table body
var th;
var tr;
var td;



function createHeader(){//define the function
var th = document.createElement('th');         
            th.innerHTML="TouchPoint Name";
            toptouchpointTable.appendChild(th);
            th = document.createElement('th'); 
            th.innerHTML= "Response"
            toptouchpointTable.appendChild(th);
            th = document.createElement('th'); 
            th.innerHTML= "Score"
            toptouchpointTable.appendChild(th);
            //touchpointTable.appendChild(thead);            
            //touchpointTable.appendChild(tbody);
}
 function createRow()//define the function
 {
containerToAppendTo.append(toptouchpointTable)
for(var i=0;i<topdata.length;i++){
    
            tr = document.createElement('tr'),
            //for data_name
            td= document.createElement('td');
            td.innerHTML=topdata[i].data_name;
            tr.appendChild(td);

            //for data_response
            td = document.createElement('td')
            td.innerHTML=topdata[i].data_response;
            tr.appendChild(td);
            //for data_score
            td = document.createElement('td');
            td.innerHTML=topdata[i].data_score;
            tr.appendChild(td);
            
            var div = document.createElement("div");//creating a div for box
            div.id= "color_" + i;//creating div id
            var colorByScore = (topdata[i].data_score >= 20) ? "green": "red";//apply the condition
            div.setAttribute("style","background: " + colorByScore)
            div.style.width = "6px";//set the width
            div.style.height = "20px";//set the height
            div.style.margin = "4px";//set the margin
            tr.appendChild(div);//append the div in table row
            tbody.appendChild(tr); //append the table row in table body  

}
  toptouchpointTable.appendChild(thead);//add the table header in toptouchpointTable
  toptouchpointTable.appendChild(tbody);//add the table body in toptouchpointTable
  containerToAppendTo.append(toptouchpointTable);//add the toptouchpointTable in container
}
  return {
    tableElement: function(data5, tableContainer) {//exposed the data
    tableElement(data5, tableContainer);
    }
 };
})();

// tableElement();

// createtable();
