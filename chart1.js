//alert("sonam");
var chartbox1 = (function() {//create a function and store the seprate variable
var chartContainer2 = null;

function createlinechart(chartdata2, chartContainer2){//create a closure function and pass the parameter
console.log(chartdata2);//console the chartdata2
createChart(chartdata2);//call the function and pass the data

}

function createChart(chartdata2){//define the function
	var chartconfiguration = {//create a variable
       animationEnabled: true,//perform in animation in chart
       backgroundColor:"#292e33",//set the background olor in chart
       title: {//display the title
            display: true,//display the chart
            text: 'Overall Payment Experience',//this is text for display the chart
            fontColor: "gray",//set the font color
        },
	   axisY:{
		  includeZero: false//this is display 0 or not in chart
	},
	data: [{        
		type: "line",//this is define the type of the graph
		color:"blue", //display the line color in chart      
		dataPoints: chartdata2
            
	}]
	
	
};
var chart = new CanvasJS.Chart("chartContainer2",chartconfiguration);
chart.render();
}
return {
    createlinechart: function(chartdata2,chartContainer2) {
      createlinechart(chartdata2,chartContainer2);
      }
 };
})(); 
