var chartbox3 = (function() {
var chartContainer4 = null;
//var datasets2 = [];
function createlinechart(chartdata4, chartContainer4){
console.log(chartdata4);
createChart(chartdata4);

}

function createChart(chartdata4){
//var chart = new CanvasJS.Chart("chartContainer", {
	var chartconfiguration = {
       animationEnabled: true,
	/*theme: "light2",*/
       backgroundColor:"#292e33",
        //label:"Ease of Finding Premium Payment Link on Website ";
       title: {
            display: true,
            text: 'Overall Payment Experience',
            fontColor: "gray",
        },
	   axisY:{
		  includeZero: false
	},
	data: [{        
		type: "line",
		color:"blue",       
		dataPoints: chartdata4
            
	}]
	
	
};
var chart = new CanvasJS.Chart("chartContainer4",chartconfiguration);
chart.render();
}
return {
    createlinechart: function(chartdata4,chartContainer4) {
      createlinechart(chartdata4,chartContainer4);
      }
 };
})(); 
