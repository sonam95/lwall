//alert("sonam");
var dataChart = (function() {//this is a function and store into a seprated variable
var chartContainer1 = null;

function createlinechart(chartdata1, chartContainer1){//this is  a closure function
console.log(chartdata1);//console the chartdata1
createChart(chartdata1);//call the function and pss the argument
}
function createChart(chartdata1){//define function

	var chartconfiguration = {//create a variable 
       animationEnabled: true, //perform the animation
       backgroundColor:"#292e33",//it is set the background color 
       //label:"Ease of Finding Premium Payment Link on Website ";

       title: {//display the title in graph
            display: false,//display property is to used the element is to hide or display.
            text: 'Ease of Finding Premium Payment Link on Website',
            fontColor: "gray",//set the text color
        },
	   axisY:{
		  includeZero: false//this is line is define 0 is include or not in graph
	},
	data: [{        
		type: "line",//this is display the line graph
		color:"blue",   //this is display the color for graph    
		dataPoints: chartdata1
            
	}]	
};

var chart = new CanvasJS.Chart("chartContainer1",chartconfiguration);
chart.render();
}
return {
    createlinechart: function(chartdata1,chartContainer1 ) {
      createlinechart(chartdata1,chartContainer1);
      }
 };

})(); 
