//alert("sonam");
var chartbox = (function() {//create a function and store the seprate variable
var chartContainer = null;
function createlinechart(chartdata, chartContainer){//cretae a closure function and pass the parameter
console.log(chartdata);//console the chartdata
createChart(chartdata);//call the function and pass the data

}

function createChart(chartdata){//define a function
	var chartconfiguration = {//craete a variable
       animationEnabled: true,//dispaly the animation in graph
       backgroundColor:"#292e33",//set the background color

	   axisY:{
		  includeZero: false
	},
	data: [{        
		type: "line",
		color:"blue",       
		dataPoints: chartdata
            
	}]
	
	
};
var chart = new CanvasJS.Chart("chartContainer",chartconfiguration);
chart.render();
}
return {
    createlinechart: function(chartdata,chartContainer ) {
      createlinechart(chartdata,chartContainer);
      }
 };
})(); 
